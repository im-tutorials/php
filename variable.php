<?php

/**
 * Variables
 * Rules for PHP variables:
 * A variable starts with the $ sign, followed by the name of  the variable
 * A variable name must begin with a letter or the underscore character
 * A variable name can only contain alpha-numeric  characters and underscores (A-z, 0-9, and _ )
 * A variable name should not contain spaces
 * Variable names are case sensitive ($y and $Y are two  different variables)
 */
    

$txt = "Hello";
$x = 5;

?>