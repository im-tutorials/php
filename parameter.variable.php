<?php

/**
 *  Parameter Variable
 *  A parameter is a local variable whose value is  passed to the function by the calling code.
 *  Parameters are declared in a parameter list as part  of the function declaration:

 */

    function myVariable($x){
        echo $x;
        
    }

    myVariable(8); // Output 8
   

?>	
