<?php

/**
 *  Static Variable
 *  When a function is completed, all of its variables are  normally deleted. 
 *  However, sometimes you want a  local variable to not be deleted.
 *  To do this, use the static keyword when you first  declare the variable:

 */

    function myVariable(){
        static $x=0;  
        echo $x;
        $x++;
        
        
    }

    myVariable(); // Output 0
    myVariable(); // Output 1
    myVariable(); // Output 2

?>	
