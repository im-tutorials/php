PHP can generate dynamic page content
PHP can create, open, read, write, and close files on  the server
PHP can collect form data
PHP can send and receive cookies
PHP can add, delete, modify data in your database
PHP can restrict users to access some pages on  your website
PHP can encrypt data

**Overview**
 - What is PHP
 - Basic Syntax
 - Variable
 - Comment
 - Array
 - Foreach
 - If...Else
 - Switch
 - Function
 - $_GET
 - $_POST
 - $_FILES
 - Exception
 - Connection
 - Insert
 - Select
 - Update
 - Delete

Hi this is HAC, please spread the love.

**Website**: https://www.impact101.guru

**Facebook**: https://www.facebook.com/Impact101guru-106000034412431

**Youtube**: https://www.youtube.com/channel/UClob2sEffrGY3-_tNJ4q_Rw

**Files**:  https://gitlab.com/im-tutorials/php
