<?php

/**
 * Multi-dimentional arrays - Arrays containing one or more arrays.
 * An array can also contain another array as a value,  which in turn can hold other arrays as well. 
 * In such a  way we can create two- or three-dimensional arrays:

 */



    $fruits_color = [
        ['apple'=>'red'],
        ['mango'=>'yellow'],
        ['orange'=>'orange'],
    ];
    
    echo '<pre>';
    print_r($fruits_color);
    echo '</pre>';

   
  
    /***
     * Output:
     * 
    Array
    (
        [0] => Array
            (
                [apple] => red
            )

        [1] => Array
            (
                [mango] => yellow
            )

        [2] => Array
            (
                [orange] => orange
            )

    )
     */



?>