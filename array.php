<?php


/**
 * Array - stores multiple values in one single  variable:
 * In PHP, the array() function is used to create an  array.
 * Since PHP 7, [] can be used to decllare and array.
    
 */

    $fruits = ['apple', 'mango', 'orange'];
    print_r($fruits);

    // Output
    // Array ( [0] => apple [1] => mango [2] => orange )

?>