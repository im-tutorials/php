<?php

/**
 * $_POST 
 * - is used to collect  values from a form sent with method="post"
 * 
 * - Information sent from a form with the POST method  is invisible to others 
 *   and has no limits on the amount  of information to send.
 * 
 * - However, there is an 8 MB max size for the  POST method, by default 
 *   (can be changed by  setting the post_max_size in the php.ini file).

 */
?>


<form action="post.php" method="post">  
    Name: <input type="text" name="fname">  
    Age: <input type="text" name="age">
    <input type="submit" value="Submit">
</form>



<?php

$name = '';
$age = '';

if(isset($_POST['fname'])){
    $name = $_POST['fname'];
    echo 'My name is '.$name.'.';
}

if(isset($_POST['age'])){
    $age = $_POST['age'];
    echo ' My age is '.$age;
}
?>