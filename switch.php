<?php
/**
 * Switch - With switch construct, it is possible to compare the same variable (or expression)
 *  with many different values, and execute a different piece of code depending on which value it equals to.
 */

$var = 4;

switch ($var) {
    case 1:
      echo 'The value equals to 1';
    break;
   
    case 2:
      echo 'The value equals to 2';
    break;
    
    default:
      echo 'The value is greater than 2';
 }

?>