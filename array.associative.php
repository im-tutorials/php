<?php

/**
 *Associative arrays - Arrays with named keys
 */



    $fruits_color = [
        'apple'=>'red',
        'mango'=>'yellow',
        'orange'=>'orange',
    ];

    print_r($fruits_color);
    echo "<br/>";

    // OR

    $fruits_color['apple'] = 'red';
    $fruits_color['mango'] = 'yellow';
    $fruits_color['orange'] = 'orange';

    print_r($fruits_color);

    // Output:
    // Array ( [apple] => red [mango] => yellow [orange] => orange )
    // Array ( [apple] => red [mango] => yellow [orange] => orange )
    



?>