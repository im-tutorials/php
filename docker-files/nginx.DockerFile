# FROM nginx:1.15-alpine
FROM nginx

ENV NGINX_HOST=localhost
ENV NGINX_PORT=80

COPY default.conf /etc/nginx/conf.d/default.conf