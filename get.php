<?php

/**
 * $_GET 
 * - is used to collect  values in a form with method="get"
 * - Information sent from a form with the GET method is  visible to everyone 
 * - It will be displayed in the  browser's address bar and has limits on the amount  of information to send.
 */
?>


<form action="get.php" method="get">  
    Name: <input type="text" name="fname">  
    Age: <input type="text" name="age">
    <input type="submit" value="Submit">
</form>



<?php

$name = '';
$age = '';

if(isset($_GET['fname'])){
    $name = $_GET['fname'];
    echo 'My name is '.$name.'.';
}

if(isset($_GET['age'])){
    $age = $_GET['age'];
    echo ' My age is '.$age;
}
?>