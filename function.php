<?php

/**
 * A function is a block of statements that can be used repeatedly in a  program.
 * A function will not execute immediately when a page loads.
 * A function will be executed by a call to the function.
 * A user defined function declaration starts with the word "function":
 */


    // Declaration
    function sampleFunction($arg1,  $arg2="default"){
        echo $arg1.'<br/>'.$arg2;
        
    }

    // Execution
    sampleFunction("Arg1", "Arg2")



?>