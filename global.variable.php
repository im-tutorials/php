<?php

/**
 *  Global Variable
 *  A variable that is defined outside of any function, has  a global scope.
 *  Global variables can be accessed from any part of  the script, EXCEPT from within a function.
 *  To access a global variable from within a function,  use the global keyword:

 */

    $x=5; // global scope
    $y=10; // global scope

    function myVariable(){
        global $x,$y;
        $y=$x+$y;
        echo $y;
        
    }

    myVariable(); // Output 15
?>	
