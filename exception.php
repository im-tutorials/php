<?php

/**
 * Exception
 * PHP has an exception model similar to that of other programming languages. 
 * An exception can be thrown, and caught ("catched") within PHP. 
 * Code may be surrounded in a try block, to facilitate the catching of potential exceptions. 
 * Each try must have at least one corresponding catch or finally block.
 */

function check_if_number($x) {
    if (!is_numeric($x)) {
        throw new Exception( $x. '  is not a number');
    }
    return "Yes!  ". $x. " is  a number <br/>";
}

// Exception with True statement
try {
    echo check_if_number(5) . "\n";

} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "<br/>";

}
echo "<hr/>";


// Exception with False statement
try {
    echo check_if_number("string") . "\n";

} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "<br/>";

} finally {
    // Continue execution
    echo "Finally block statement.\n";
}





?>
