<?php

/**
 *  Local Variable
 *  A variable declared within a PHP function is local  and can only be accessed within that function:
 */
    function myVariable(){
        $x=5; 
        echo $x; // local scope
    }

    myVariable();

    // Output 5
?>	
