<?php
/**
 * IF-Else Statement - If you want to execute some code if a condition is true and another code 
 * if a condition is false, use the if....else statement.
 */


$var = 4;

if($var==1){
    echo 'The value equals to 1';

}else if($var==2){
    echo 'The value equals to 2';

}else{
    echo 'The value is greater than 2';

}

?>