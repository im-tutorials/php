<?php
/**
 * Indexed array - Arrays with numeric index
 */

// Index automatically assigned
$fruits = ['apple', 'mango', 'banana'];

// OR

// Index manually assigned.
$fruits[0] = 'apple';
$fruits[1] = 'mango';
$fruits[2] = 'orange';

print_r($fruits);

// Output:  
// Array ( [0] => apple [1] => mango [2] => orange )



?>