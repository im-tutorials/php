<?php

/**
 * The foreach loop works only on arrays, and is used to  loop through each key/value pair in an array. 
 * For every loop iteration, the value of the  current array element is assigned to $value 
 * (and the array pointer is moved by one) - so on the next loop iteration, 
 * you'll  be looking at the next array value.
 
 */


// Looping with indexed array
$fruits = ['apple', 'mango', 'banana'];
 
foreach ($fruits as $value){
    echo $value. '<br/>';
}

// Output:
// apple
// mango
// banana


// Looping wiith associative array
$fruits_color = [
    'apple'=>'red',
    'mango'=>'yellow',
    'orange'=>'orange',
];


foreach ($fruits as $key=>$value){
    echo 'Key: ' .$key. ' Value: '.$value. '<br/>';
}

// Output:
// Key: 0 Value: apple
// Key: 1 Value: mango
// Key: 2 Value: banana

?>