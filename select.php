<html>
<head>
    <title>PHP - Select</title>
</head>

<body>
<h1>PHP - Select</h1>
<a href="/insert.php">Add Data</a>
<hr>
<table border="1" width="60%">
    <thead>
        <tr>
            <th>No. </th>
            <th>Car Name</th>
            <th>Color</th>
            <th>Model</th>
            <th>Action</th>

            
        </tr>
    </thead>

    <tbody>
        <?php

            include 'connection.php';
            $query = "SELECT id, car_name, color, model FROM tbl_cars";
            $result = $conn->query($query);


            if ($result->num_rows > 0) {
                $num = 1;
                while($row = $result->fetch_assoc()) {
                    echo '<tr>
                        <td>'.$num.'</td>
                        <td>'.$row['car_name'].'</td>
                        <td>'.$row['color'].'</td>
                        <td>'.$row['model'].'</td>
                        <td>
                            <a href="/update.php?id='.$row['id'].'">edit</a>
                            <a href="/controller.php?id='.$row['id'].'&action=delete">delete</a>
                        </td>
                    </tr>';
                    $num++;

                }
            } else {
                echo 'No record found.<br />';
            }
            mysqli_free_result($result);
            $conn->close();


        
            
        ?>
    
    </tbody>
</table>

<?php

?>
   
</body>
</html>